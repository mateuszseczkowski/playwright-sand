﻿using System.Threading.Tasks;

namespace PlaywrightSand.PageObjects.Testpages.herokuapp
{
    //see https://playwright.dev/docs/pom/ PageObjectModel
    /*
     * Introduction#
        A page object represents a part of your web application. An e-commerce web application might have a home page, a listings page and a checkout page. Each of them can be represented by page object models.

        Page objects simplify authoring. They create a higher-level API which suits your application.

        Page objects simplify maintenance. They capture element selectors in one place and create reusable code to avoid repetition.
     */

    //!!! Page object models wrap over a Playwright Page(https://playwright.dev/docs/api/class-page/).!!!

    public abstract class BasePage : IPageObject
    {
        public abstract Task GoTo();
    }
}
