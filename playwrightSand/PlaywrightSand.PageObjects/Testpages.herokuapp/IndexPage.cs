﻿using System.Threading.Tasks;
using Microsoft.Playwright;

namespace PlaywrightSand.PageObjects.Testpages.herokuapp
{
    public class IndexPage : BasePage
    {
        public string Location { get; private set; } = "index.html";

        private readonly IPage page;

        private string navigationTableLink(string text) => $"a:has-text(\"{text}\")";

        public IndexPage(IPage page)
        {
            this.page = page;
        }

        public override async Task GoTo()
        {
            //this may be overwritten by the interceptor/proxy class and only 'index' used - an own page class implementation to avoid some duplications needed for more custom stuff
            await page.GotoAsync(string.Format("https://testpages.herokuapp.com/styled/{0}", Location)
                , new PageGotoOptions() { WaitUntil = WaitUntilState.DOMContentLoaded }); 
        }

        public async Task NavigateTo(string text)
        {
            await page.ClickAsync(navigationTableLink(text));
        }
    }
}
