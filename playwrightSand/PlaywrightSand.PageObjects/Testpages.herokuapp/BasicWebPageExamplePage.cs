﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Playwright;

namespace PlaywrightSand.PageObjects.Testpages.herokuapp
{
    public class BasicWebPageExamplePage : BasePage
    {
        public string Location { get; private set; } = "basic-web-page-test.html";

        private readonly IPage page;

        private Task<IReadOnlyList<IElementHandle>> paragraphs => page.QuerySelectorAllAsync("p[id*='para']");

        public async Task<int> GetParagraphsCountAsync() => (await paragraphs).Count;        

        public BasicWebPageExamplePage(IPage page)
        {
            this.page = page;
        }

        public override async Task GoTo()
        {
            //this may be overwritten by the interceptor class and only 'index' used - an own page class implementation
            await page.GotoAsync(string.Format("https://testpages.herokuapp.com/styled/{0}", Location)
                , new PageGotoOptions() { WaitUntil = WaitUntilState.DOMContentLoaded });
        }
    }
}
