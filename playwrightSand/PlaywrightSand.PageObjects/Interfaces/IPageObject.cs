﻿using System.Threading.Tasks;

namespace PlaywrightSand.PageObjects.Testpages.herokuapp
{
    public interface IPageObject
    {
        Task GoTo();
    }
}