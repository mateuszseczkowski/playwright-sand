﻿using BoDi;
using Microsoft.Playwright;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace playwrightSand.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private ObjectContainer _objectContainer;
        private ScenarioContext _scenarioContext;
        private FeatureContext _featureContext;

        public static string AssemblyFolder;
        public static string PathToScreensFolder;
        
        public Hooks(ObjectContainer objectContainer, FeatureContext featureContext, ScenarioContext scenarioContext)
        {
            _objectContainer = objectContainer;
            _scenarioContext = scenarioContext;
            _featureContext = featureContext;

            AssemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            PathToScreensFolder = Path.Combine(AssemblyFolder, "Screens\\");
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
        }

        [BeforeScenario("playwright")]
        public async Task PlaywrightInitBeforeScenario()
        {
            var launchOptions = new BrowserTypeLaunchOptions()
            {
                Headless = false
            };

            var playwright = await Playwright.CreateAsync();
            var browser = await playwright.Chromium.LaunchAsync(launchOptions);
            var page = await browser.NewPageAsync();

            _objectContainer.RegisterInstanceAs(playwright);
            _objectContainer.RegisterInstanceAs(browser);
            _objectContainer.RegisterInstanceAs(page);
        }

        [AfterScenario("playwright")]
        public async Task PlaywrightTeardownAfterScenarioAsync()
        {
            var screenshotOptions = new PageScreenshotOptions()
            {
                Path = PathToScreensFolder + _scenarioContext.ScenarioInfo.Title + ".png"
            };

            var page = _objectContainer.Resolve<IPage>();
            await page.ScreenshotAsync(screenshotOptions);
            await page.CloseAsync();

            var browser = _objectContainer.Resolve<IBrowser>();
            await browser.DisposeAsync();

            var playwright = _objectContainer.Resolve<IPlaywright>();
            playwright?.Dispose();
        }

        [AfterScenario]
        public void AfterScenario()
        {
        }
    }
}