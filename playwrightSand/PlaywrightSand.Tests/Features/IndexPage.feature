﻿Feature: Index page
TestAPP from https://testpages.herokuapp.com/styled/index.html
Backup fork https://github.com/mateuszseczkowski/TestingApp

#@selenium
@playwright
Scenario: Open a page and go to Basic Web Page Example
	Given I have accessed a TestAPP web page
	When I go to 'Basic Web Page Example'
	Then A page with '2' paragraphs is opened