﻿using BoDi;
using Microsoft.Playwright;

namespace playwrightSand.Steps
{
    public class BaseStep
    {
        protected IObjectContainer _objectContainer;

        protected IPage Page => _objectContainer.Resolve<IPage>();

        public BaseStep(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        } 
    }
}
