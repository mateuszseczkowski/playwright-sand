﻿using BoDi;
using PlaywrightSand.PageObjects.Testpages.herokuapp;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using FluentAssertions;

namespace playwrightSand.Steps
{
    [Binding]
    public sealed class IndexPageSteps : BaseStep
    {
        private readonly ScenarioContext _scenarioContext;
        private IndexPage indexPage;
        private BasicWebPageExamplePage basicWebPageExamplePage;

        public IndexPageSteps(IObjectContainer objectContainer, ScenarioContext scenarioContext) : base(objectContainer)
        {
            _scenarioContext = scenarioContext;

            indexPage = new IndexPage(Page);
            basicWebPageExamplePage = new BasicWebPageExamplePage(Page);
        }

        [Given(@"I have accessed a TestAPP web page")]
        public async Task GivenIHaveAccessedATestAPPWebPage()
        {
            await indexPage.GoTo();
        }

        [When(@"I go to '(.*)'")]
        public async Task WhenIGoTo(string pageTitle)
        {
            await indexPage.NavigateTo(pageTitle);
        }

        [Then(@"A page with '(.*)' paragraphs is opened")]
        public async Task ThenAPageWithParagraphsIsOpened(int countExpected)
        {
            var paragraphsOnPageCount = await basicWebPageExamplePage.GetParagraphsCountAsync();

            paragraphsOnPageCount.Should().Equals(countExpected);
        }
    }
}
